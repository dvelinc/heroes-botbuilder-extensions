'use strict';

module.exports.BotStorage = require('./src/BotStorage');
module.exports.PostgresClient = require('./src/PostgresClient');
module.exports.FacebookBotConnector = require('./src/FacebookBotConnector');
module.exports.HistoryMiddleware = require('./src/middleware/HistoryMiddleware');
module.exports.SendDelayMiddleware = require('./src/middleware/SendDelayMiddleware');

module.exports.analytics = {};
module.exports.analytics.Bonobo = require('./src/analytics/Bonobo');
