## Facebook Connector

### Custom features

#### Attachment reuse

```javascript
const videoCard = new builder.VideoCard(session)
      .title('Video Card')
      .subtitle('Microsoft Band')
      .media([
        builder.CardMedia.create(session, 'http://video.ch9.ms/ch9/08e5/6a4338c7-8492-4688-998b-43e164d908e5/thenewmicrosoftband2_mid.mp4')
      ])
      .autoloop(true)
      .autostart(false)
      .shareable(true);
      
// set reusable flag for built-in types (also works with ImageCards)
videoCard.data.reusable = true;

const msg = new builder.Message(session)
      .attachments([
        videoCard,
      ]);
      
// set reusable flag in custom attachment
const imageAttachment = {
    contentType: 'image/jpeg',
    contentUrl: 'https://www.swell.wtf/img/heroes/luisa-lion-107f8d8054.jpg',
    reusable: true
};

const msg2 = new builder.Message(session)
      .attachments([
        imageAttachment,
      ]);
```

#### Url Button extensions

```javascript
const urlButton = builder.CardAction.openUrl(session, 'https://en.wikipedia.org/wiki/Space_Needle');

// set FB specific url button properties
urlButton.data.messenger_extensions = true;
urlButton.data.webview_height_ratio = 'compact';
urlButton.data.fallback_url = 'https://en.wikipedia.org/wiki/Space_Needle?fallback';

// attach url button to hero card default action
const msg = new builder.Message(session)
      .attachments([
        new builder.HeroCard(session)
          .title('Hero Card')
          .subtitle('The Space Needle is an observation tower in Seattle, Washington, a landmark of the Pacific Northwest, and an icon of Seattle.')
          .images([
            builder.CardImage.create(session, 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7c/Seattlenighttimequeenanne.jpg/320px-Seattlenighttimequeenanne.jpg')
          ])
          .tap(urlButton)
      ]);

// attach url button to hero card buttons
const msg2 = new builder.Message(session)
      .attachments([
        new builder.HeroCard(session)
          .title('Hero Card')
          .subtitle('The Space Needle is an observation tower in Seattle, Washington, a landmark of the Pacific Northwest, and an icon of Seattle.')
          .images([
            builder.CardImage.create(session, 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7c/Seattlenighttimequeenanne.jpg/320px-Seattlenighttimequeenanne.jpg')
          ])
          .buttons([
            urlButton,
            builder.CardAction.imBack(session, 'select:101', 'Select'),
          ])
      ]);
```

#### Share button extension

```javascript
const shareButton = { type: 'shareCard' };

// attach url button to hero card buttons
const msg = new builder.Message(session)
      .attachments([
        new builder.HeroCard(session)
          .title('Hero Card')
          .subtitle('The Space Needle is an observation tower in Seattle, Washington, a landmark of the Pacific Northwest, and an icon of Seattle.')
          .images([
            builder.CardImage.create(session, 'https://upload.wikimedia.org/wikipedia/commons/thumb/7/7c/Seattlenighttimequeenanne.jpg/320px-Seattlenighttimequeenanne.jpg')
          ])
          .buttons([
            shareButton,
          ])
      ]);
```