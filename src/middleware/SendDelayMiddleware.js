'use strict';

/**
 * usage:
 * const sendDelayMiddleware = botbuilderExtensions.SendDelayMiddleware(pgClient);
 * ...
 * const bot = new builder.UniversalBot(connector);
 * bot.use(
 *  {
 *   send: sendDelayMiddleware.send,
 *  }
 * );
 * @param _delay int delay in seconds
 * @returns { send }
 * @constructor
 */
function SendDelayMiddleware(_delay) {
  const delay = _delay || 0;

  const sendDelayMiddleware = {};

  sendDelayMiddleware.send = function send(message, next) {
    if (message.type === 'typing') {
      next();
      return;
    }

    if (delay <= 0) {
      next();
      return;
    }

    setTimeout(next, delay * 1000);
  };

  return sendDelayMiddleware;
}

module.exports = SendDelayMiddleware;
