'use strict';

/**
 * usage:
 * const historyMiddleware = builderExtensions.HistoryMiddleware(pgClient);
 * ...
 * const bot = new builder.UniversalBot(connector);
 * bot.set('storage', sessionStorage);
 * bot.use(
 *  {
 *   send: historyMiddleware.send,
 *   botbuilder: historyMiddleware.botbuilder,
 *  }
 * );
 * Incoming:
 * 1. receive()
 * 2. get session
 * 3. botbuilder()
 * 4. routing
 * ---
 * Outgoing:
 * 1. save session
 * 2. send()
 * @param storageClient
 * @returns { botbuilder, send }
 * @constructor
 */
function HistoryMiddleware(storageClient) {
  if (!storageClient) {
    throw new Error('No storage client specified');
  }

  const historyMiddleware = {};

  // use botbuilder middleware instead of receive, because here we already have the session
  historyMiddleware.botbuilder = function botbuilder(session, next) {
    const pgUserId = session.userData.pgUserId;
    session.message.address.pgUserId = pgUserId; // store postgres ID to retrieve it in send() function
    const message = JSON.parse(JSON.stringify(session.message)); // clone message
    // get rid of bloat
    delete message.address;
    delete message.user;
    delete message.source;
    delete message.agent;
    storageClient.insertHistory(pgUserId, 'in', message)
      .catch((error) => {
        console.warn('HISTORY', 'Incoming message error: ', error);
      });
    next();
  };

  historyMiddleware.send = function send(message, next) {
    if (message.type === 'typing') {
      next();
      return;
    }

    if (!message.address || !message.address.pgUserId) {
      console.warn('HISTORY', 'No address.userId available', message);
      next();
      return;
    }
    const messageCopy = JSON.parse(JSON.stringify(message)); // clone message
    const pgUserId = message.address.pgUserId; // == postgres user ID we stored in botbuilder()
    // get rid of bloat
    delete messageCopy.address;
    delete messageCopy.user;
    delete messageCopy.source;
    delete messageCopy.agent;
    storageClient.insertHistory(pgUserId, 'out', messageCopy)
      .catch((error) => {
        console.warn('HISTORY', 'Outgoing message error: ', error);
      });
    next();
  };

  return historyMiddleware;
}

module.exports = HistoryMiddleware;
