'use strict';

const rp = require('request-promise');

function BonoboClient(token, url, config) {
  const that = this;
  that.token = token;
  that.url = url;
  that.debug = !!config.debug;
  rp.debug = !!config.debugRequest;
  that.platform = config.platform === 'messenger' ? 'facebook' : config.platform;
  // we could set whatever platform identifier we want (e.g. for prepareBody)

  function prepareBody(data) {
    const body = {
      platform: that.platform,
      token: that.token,
      date_time: new Date().getTime(),
    };
    body.intents_list = data.intentList || [];
    delete data.intentList;
    body.full_json = data;
    return body;
  }

  /**
   * track incoming data
   * @param data object incoming/outgoing message from/to platform
   * and you can add a custom intentList [] you want to track
   * @returns Promise response
   */
  that.track = function track(data) {
    if (that.debug) {
      console.log(`Bonobo:track: ${that.url}`);
      console.log('Bonobo:track.data:', JSON.stringify(data, null, 2));
    }

    return rp({
      uri: that.url,
      method: 'POST',
      json: prepareBody(data),
    });
  };
}

/**
 * @param token
 * @param config object {
 * platform => facebook, viber, ...
 * debug => see every outgoing data,
 * debugRequest => to set the request-promise library to debug
 * }
 * @returns {BonoboClient}
 */
module.exports = function Bonobo(token, config) {
  if (!token) {
    throw new Error('YOU MUST SUPPLY A TOKEN FOR BONOBO!');
  }
  if (!config && !config.platform) {
    throw new Error('YOU MUST SUPPLY A PLATFORM FOR BONOBO!');
  }
  const uri = 'https://api.bonobo.ai/apiv1/addmessage/';
  return new BonoboClient(token, uri, config || {});
};
