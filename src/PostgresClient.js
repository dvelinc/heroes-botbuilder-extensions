'use strict';

const pg = require('pg-promise');
const constants = require('./CONST');
const Promise = require('bluebird');

/**
 * @param options object { host, port, database, user, password, schema }
 * @constructor
 */
function PostgresClient(options) {
  if (!options.host || !options.user || !options.password || !options.database || !options.schema) {
    throw Error('Database host, user, password, database, schema are mandatory');
  }
  this.options = options;

  const initOptions = {
    promiseLib: Promise,
    connect: function connect(client, dc, isFresh) { // eslint-disable-line
      // const cp = client.connectionParameters;
      // console.log('Connected to database:', cp.database, dc, isFresh);
    },
    disconnect: function disconnect(client, dc) { // eslint-disable-line
      // const cp = client.connectionParameters;
      // console.log('Disconnecting from database:', cp.database, dc);
    },
    error: function error(err, e) {
      if (e.cn) {
        // A connection-related error;
        //
        // Connections are reported back with the password hashed,
        // for safe errors logging, without exposing passwords.
        console.error('PostgresClient: CN:', e.cn);
      }
      if ((err.message || err).includes('clients" does not exist')) return;
      if (e.query) {
        // query string is available
        console.error('PostgresClient: query string: ', e.query);
        if (e.params) {
          // query parameters are available
          console.error('PostgresClient: query params: ', e.params);
        }
      }
      if (e.ctx) {
        // occurred inside a task or transaction
        console.error('PostgresClient: CTX: ', e.ctx);
      }
      console.error('PostgresClient: pg error:', err.message || err);
    },
  };
  const pgp = pg(initOptions);
  const connectionConfig = this.options;
  this.db = pgp(connectionConfig);
}

/**
 * connect the db automatically by querying it
 * dont use the db.connect() it is buggy according to the creator of the library
 * @returns Promise
 */
PostgresClient.prototype.connect = function connect() {
  return this.db.one('SELECT version()');
};

PostgresClient.prototype.saveStorageData = function saveStorageData(chatBotPlatform, chatBotUserId, data) {
  if (data[constants.Fields.IdField] == null) {
    console.error('Session was not initialized properly, dvel user id missing');
    return Promise.reject('Session was not initialized properly, dvel user id missing');
  }
  const updates = [];
  if (!data[constants.Fields.UserDataField + constants.hash]
    || data[constants.Fields.UserDataField + constants.hash] !== JSON.stringify(
      data[constants.Fields.UserDataField])) {
    updates.push({ fieldName: 'user_data', value: data[constants.Fields.UserDataField] });
    data[constants.Fields.UserDataField + constants.hash] = data[constants.Fields.UserDataField] ? JSON.stringify(
      data[constants.Fields.UserDataField]) : null;
  }

  if (!data[constants.Fields.PrivateConversationDataField + constants.hash]
    || data[constants.Fields.PrivateConversationDataField + constants.hash] !== JSON.stringify(
      data[constants.Fields.PrivateConversationDataField])) {
    updates.push({
      fieldName: 'private_conversation_data',
      value: data[constants.Fields.PrivateConversationDataField],
    });
    data[constants.Fields.PrivateConversationDataField + constants.hash] =
      data[constants.Fields.PrivateConversationDataField] ? JSON.stringify(
        data[constants.Fields.PrivateConversationDataField]) : null;
  }

  if (updates.length > 0) {
    let updateStatement = '';
    const updateValues = [];
    let i = 0;
    updates.forEach((v) => {
      i += 1;
      updateStatement += `${v.fieldName}=$${i}, `;
      updateValues.push(v.value);
    });
    updateStatement = updateStatement.substr(0, updateStatement.length - 2);
    const sqlStatement = `UPDATE ${this.options.schema}.${constants.userTableName} ` +
      `SET ${updateStatement} WHERE id = ${data[constants.Fields.IdField]}`;
    return this.db.none(sqlStatement, updateValues);
  }

  return Promise.resolve();
};

PostgresClient.prototype.getStorageData = function getStorageData(chatBotPlatform, chatBotUserId) {
  return this.db.oneOrNone(
    'SELECT id, user_data, private_conversation_data ' +
    `FROM ${this.options.schema}.${constants.userTableName} 
    WHERE chat_bot_platform=$1 AND chat_bot_user_id=$2`,
    [chatBotPlatform, chatBotUserId]
  )
    .then((selectUserResult) => {
      if (selectUserResult) {
        return selectUserResult;
      }
      return this.db.one(
        `INSERT INTO ${this.options.schema}.${constants.userTableName} ` +
        '(chat_bot_user_id, chat_bot_platform, user_data, private_conversation_data) ' +
        'VALUES ($1, $2, null, null) RETURNING id, user_data, private_conversation_data',
        [chatBotUserId, chatBotPlatform]
      )
        .then((insertUserResult) => {
          return insertUserResult;
        });
    });
};

PostgresClient.prototype.insertHistory = function insertHistory(pgUserId, direction, message) {
  const values = [
    pgUserId,
    direction,
    message,
  ];
  return this.db.none(
    `INSERT INTO ${this.options.schema}.${constants.historyTableName} ` +
    '(chat_bot_user_id, direction, message) ' +
    'VALUES ($1, $2, $3)', values
  );
};

PostgresClient.prototype.insertAttachmentCache = function insertAttachmentCache(chatBotPlatform,
  attachmentUrl, cacheId) {
  const values = [
    chatBotPlatform,
    attachmentUrl,
    cacheId,
  ];
  return this.db.none(
    `INSERT INTO ${this.options.schema}.${constants.attachmentCacheTableName} ` +
    '(chat_bot_platform, attachment_url, cache_id) ' +
    'VALUES ($1, $2, $3)', values
  );
};

PostgresClient.prototype.getAttachmentCacheId = function getAttachmentCacheId(chatBotPlatform, attachmentUrl) {
  return this.db.oneOrNone(
    'SELECT cache_id FROM ' +
    `${this.options.schema}.${constants.attachmentCacheTableName} 
    WHERE chat_bot_platform=$1 AND attachment_url=$2`,
    [chatBotPlatform, attachmentUrl]
  );
};

PostgresClient.prototype.getUserByPgUserId = function getUserByPgUserId(pgUserId) {
  return this.db.oneOrNone(
    'SELECT * FROM ' +
    `${this.options.schema}.${constants.userTableName} WHERE id=$1`,
    [pgUserId]
  );
};

PostgresClient.prototype.getUserByChatBotUserId = function getUserByChatBotUserId(chatBotUserId) {
  return this.db.oneOrNone(
    'SELECT * FROM ' +
    `${this.options.schema}.${constants.userTableName} WHERE chat_bot_user_id=$1`,
    [chatBotUserId]
  );
};

PostgresClient.prototype.getAllChatBotUsers = function getAllChatBotUsers() {
  return this.db.any(
    `SELECT *
    FROM ${this.options.schema}.${constants.userTableName}`
  );
};

PostgresClient.prototype.getUserData = function getUserData(pgUserId) {
  return this.db.oneOrNone(
    `SELECT user_data
    FROM ${this.options.schema}.${constants.userTableName}
    WHERE id=$1`,
    [pgUserId]
  )
    .then((user) => {
      if (!user) {
        throw new Error('user not found');
      }
      return user.user_data;
    });
};

PostgresClient.prototype.updateUserData = function updateUserData(pgUserId, jsonPath, newValues) {
  return this.db.none(
    `UPDATE ${this.options.schema}.${constants.userTableName}
    SET user_data=jsonb_set(user_data, $1, $2)
    WHERE id=$3`,
    [`{${jsonPath}}`, newValues, pgUserId]);
};

PostgresClient.prototype.updateUserDataByChatBotUserId = function updateUserDataByChatBotUserId(chatBotUserId, jsonPath,
  newValues) { // eslint-disable-line max-len
  return this.db.none(
    `UPDATE ${this.options.schema}.${constants.userTableName}
    SET user_data=jsonb_set(user_data, $1, $2)
    WHERE chat_bot_user_id=$3`,
    [`{${jsonPath}}`, newValues, chatBotUserId]);
};

PostgresClient.prototype.getRemindersForDate = function getRemindersForDate(date) {
  const tickDate = typeof date === 'string' ? new Date(date) : date;
  const day = tickDate.getDay();
  const hh = tickDate.getHours();
  const mm = tickDate.getMinutes();
  // we save the job tick-time '00:00' as '0'
  const hhmm = (!hh && !mm) ? '0' : `${hh}${mm < 10 ? '0' : ''}${mm}`;

  return this.db.manyOrNone(
    `SELECT id, user_data -> 'reminder' -> '$1' AS reminders
    FROM ${this.options.schema}.${constants.userTableName}
    WHERE user_data -> 'reminder' ->> '$1' LIKE '%"$2:raw":%'`,
    [day, hhmm]
  );
};

PostgresClient.prototype.insertInstagramUser = function insertInstagramUser(pgUserId, instagramUserId, accessToken) {
  return this.db.one(
    `INSERT INTO ${this.options.schema}.${constants.instagramUserTableName}
    (user_id, instagram_user_id, access_token, updated_at)
    VALUES ($1, $2, $3, (now() AT TIME ZONE 'utc')) RETURNING user_id`,
    [pgUserId, instagramUserId, accessToken]
  )
    .then((insertedUser) => {
      return insertedUser.id;
    });
};

PostgresClient.prototype.getInstagramUserByPgUserId = function getInstagramUserByPgUserId(pgUserId) {
  return this.db.oneOrNone(
    `SELECT *
    FROM ${this.options.schema}.${constants.instagramUserTableName} 
    WHERE user_id=$1`,
    [pgUserId]
  );
};

PostgresClient.prototype.getInstagramUserByInstagramUserId = function getInstagramUserByInstagramUserId(instagramUserId) { // eslint-disable-line max-len
  return this.db.oneOrNone(
    `SELECT *
    FROM ${this.options.schema}.${constants.instagramUserTableName} 
    WHERE instagram_user_id=$1`,
    [instagramUserId]
  );
};

PostgresClient.prototype.getInstagramUsersBySubscriptionId = function getInstagramUsersBySubscriptionId(subscriptionId) { // eslint-disable-line max-len
  return this.db.manyOrNone(
    `SELECT *
    FROM ${this.options.schema}.${constants.instagramUserTableName} 
    WHERE subscription_id=$1`,
    [subscriptionId]
  );
};

PostgresClient.prototype.getAllInstagramUsers = function getAllInstagramUsers() {
  return this.db.manyOrNone(
    `SELECT * FROM ${this.options.schema}.${constants.instagramUserTableName}`
  );
};

PostgresClient.prototype.updateInstagramUserCredentials = function updateInstagramUserCredentials(pgUserId,
  instagramUserId, accessToken) { // eslint-disable-line max-len
  return this.db.none(
    `UPDATE ${this.options.schema}.${constants.instagramUserTableName}
    SET instagram_user_id=$1, access_token=$2, is_expired=false, updated_at=(now() AT TIME ZONE 'utc')
    WHERE user_id=$3`,
    [instagramUserId, accessToken, pgUserId]
  );
};

PostgresClient.prototype.updateInstagramUserFeedData = function updateInstagramUserFeedData(pgUserId, feedData) {
  return this.db.none(
    `UPDATE ${this.options.schema}.${constants.instagramUserTableName}
    SET feed_data=$1, updated_at=(now() AT TIME ZONE 'utc')
    WHERE user_id=$2`,
    [feedData, pgUserId]
  )
  // append the pgUserId to a possible error object to handle the error gracefully
    .catch((error) => {
      error.pgUserId = pgUserId;
      throw error;
    });
};

PostgresClient.prototype.updateInstagramUserSubscriptionId = function updateInstagramUserSubscriptionId(pgUserId,
  subscriptionId) { // eslint-disable-line max-len
  return this.db.none(
    `UPDATE ${this.options.schema}.${constants.instagramUserTableName}
    SET subscription_id=$1, updated_at=(now() AT TIME ZONE 'utc')
    WHERE user_id=$2`,
    [subscriptionId, pgUserId]
  );
};

PostgresClient.prototype.expireInstagramUser = function expireInstagramUser(pgUserId) {
  return this.db.none(
    `UPDATE ${this.options.schema}.${constants.instagramUserTableName}
    SET is_expired=true, updated_at=(now() AT TIME ZONE 'utc')
    WHERE user_id=$1`,
    [pgUserId]
  );
};

PostgresClient.prototype.deleteInstagramUserByPgUserId = function deleteInstagramUserByPgUserId(pgUserId) {
  return this.db.none(
    `DELETE FROM ${this.options.schema}.${constants.instagramUserTableName}
    WHERE user_id=$1`,
    [pgUserId]
  );
};

PostgresClient.prototype.resetAllInstagramImagesUsedIndices = function resetAllInstagramImagesUsedIndices() {
  return this.db.none(
    `UPDATE ${this.options.schema}.${constants.userTableName}
    SET user_data=jsonb_set(user_data, '{usedIndices, instagramImages}', '[]')`
  );
};

PostgresClient.prototype.expireAuthTokenOfChatBotUser = function expireAuthTokenOfChatBotUser(pgUserId, tokenName) {
  return this.db.none(
    `UPDATE ${this.options.schema}.${constants.userTableName}
    SET user_data=jsonb_set(user_data, '{auth, $1:raw}', '""')
    WHERE id=$2`,
    [tokenName, pgUserId]
  );
};

PostgresClient.prototype.insertMessengerCode = function insertMessengerCode(options) {
  const insertColumns = ['creator_id'];
  const values = [options.pgUserId];
  // add optional parameters to the query
  if (options.payload) {
    insertColumns.push('payload');
    values.push(options.payload);
  }
  if (options.valid_till) {
    insertColumns.push('valid_till');
    values.push(options.valid_till);
  }
  if (options.single_use) {
    insertColumns.push('single_use');
    values.push(true);
  }
  // add prepared statement flags for each parameter to the query
  const preparedValues = [];
  values.forEach((value, index) => {
    preparedValues.push(`$${index + 1}`);
  });
  return this.db.one(
    `INSERT INTO ${this.options.schema}.${constants.messengerCodeTableName}
    (${insertColumns.join(',')})
    VALUES (${preparedValues.join(',')}) RETURNING id`,
    values
  )
    .then((insertedCode) => {
      return insertedCode.id;
    });
};

PostgresClient.prototype.getMessengerCodeByPayload = function getMessengerCodeByPayload(payload) {
  return this.db.oneOrNone(
    `SELECT *
    FROM ${this.options.schema}.${constants.messengerCodeTableName} 
    WHERE payload=$1`,
    [payload]
  );
};

PostgresClient.prototype.expireMessengerCode = function expireMessengerCode(id) {
  return this.db.none(
    `UPDATE ${this.options.schema}.${constants.messengerCodeTableName}
    SET is_expired=true
    WHERE id=$1`,
    [id]
  );
};

PostgresClient.prototype.addMessengerCodeUsage = function addMessengerCodeUsage(id, pgUserId) {
  return this.db.none(
    `UPDATE ${this.options.schema}.${constants.messengerCodeTableName}
    SET usages=usages || '{$2:raw}'
    WHERE id=$1`,
    [id, pgUserId]
  );
};

PostgresClient.prototype.insertClient = function insertClient(options) {
  console.log('insertClient options', options);
  return this.db.one(
    `INSERT INTO ${this.options.schema}.${constants.clientsTableName}
    (page_id, page_token, creator_id, is_active)
    VALUES ($1, $2, $3, true) RETURNING id`,
    [options.pageId, options.pageToken, options.pgUserId]
  )
    .then((insertedClient) => {
      return insertedClient.id;
    });
};

PostgresClient.prototype.getClientByBotId = function getClientByBotId(pageId) {
  return Promise.resolve(null); // do nothing here
  // return this.db.oneOrNone(
  //   `SELECT *
  //   FROM ${this.options.schema}.${constants.clientsTableName}
  //   WHERE page_id=$1`,
  //   [pageId]
  // )
  //   .catch(() => {
  //     // ignore errors
  //     return null;
  //   });
};

PostgresClient.prototype.updateClientPageToken = function updateClientPageToken(options) {
  return this.db.none(
    `UPDATE ${this.options.schema}.${constants.clientsTableName}
    SET page_token=$1
    WHERE page_id=$2`,
    [options.pageToken, options.pageId]
  );
};

PostgresClient.prototype.deactivateClient = function deactivateClient(pageId) {
  return this.db.none(
    `UPDATE ${this.options.schema}.${constants.clientsTableName}
    SET is_active=false
    WHERE page_id=$1`,
    [pageId]
  );
};

module.exports = PostgresClient;
