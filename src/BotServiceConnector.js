'use strict';

const builder = require('botbuilder');
const Promise = require('bluebird');
const rp = require('request-promise');

/**
 * Parent class for all direct connectors
 * Connects Microsoft Bot Framework directly to Bot APIs
 * @param settings object { appId, appPassword, verifyToken, secret }
 */
class BotServiceConnector extends builder.ChatConnector {
  /**
   * @param settings object { appId, appPassword }
   * @param storageClient
   * @param platformName
   * @constructor
   */
  constructor(settings, storageClient, platformName) {
    super(settings);
    this.settings = settings;
    this.storageClient = storageClient;
    this.platformName = platformName || 'universal';

    if (!storageClient) {
      throw new Error('No storage client specified');
    }
  }

  /**
   * Dispatch messages to ChatConnector
   * @param messages
   */
  dispatch(messages) {
    this.onEventHandler(messages);
  }

  /**
   * Post messages to Bot API
   * @param msg
   * @param cb
   */
  postMessage(msg, cb) {
    super.postMessage(msg, cb);
  }

  /**
   * Validate request integrity
   * @param req
   * @param body
   * @returns bool
   */
  validateRequestIntegrity(req, body) { // eslint-disable-line class-methods-use-this, no-unused-vars
    return true;
  }

  /**
   * Reformat incoming data to Microsoft Bot Framework format
   * @param messageData
   * @returns Array
   */
  reformatRequestData(messageData) { // eslint-disable-line class-methods-use-this, no-unused-vars
    return [];
  }

  /**
   * Allow sub classes to handle GET requests
   * @param req
   * @param res
   */
  handleGet(req, res) { // eslint-disable-line class-methods-use-this
    res.send('GET requests not implemented');
  }

  /**
   * Promisified request
   * @param options
   * @returns Promise
   */
  static requestp(options) {
    options.resolveWithFullResponse = true;
    options.simple = false; // don't fail if status != 200
    return rp(options)
      .then((res) => {
        if (res.statusCode >= 400) {
          const error = new Error('Error while sending message');
          let reqBody;
          // check the body we sent before in the request
          if (res.request.body) {
            reqBody = JSON.parse(res.request.body);
          } else if (res.request.formData && res.request.formData.recipient) {
            reqBody = { recipient: JSON.parse(res.request.formData.recipient) };
          }
          if (reqBody && reqBody.recipient) {
            error.recipientId = reqBody.recipient.id;
          }
          error.body = res.body;
          error.status = res.statusCode;
          if (res.body) { // set original response error as error message
            error.message = res.body;
          }
          throw error;
        } else if (res.statusCode !== 200) {
          throw new Error(`Unexpected status code: ${res.statusCode}`);
        }
        return res.body;
      })
      .catch((error) => {
        if (error.code === 200) {
          // (#200) This person isn\'t available right now.
          return;
        }
        console.error('BotServiceConnector.requestp', JSON.stringify(error));
      });
  }

  /**
   * Save attachment id
   * @param url
   * @param id
   */
  saveAttachmentId(url, id) {
    this.storageClient.insertAttachmentCache(this.platformName, url, id)
      .catch((error) => {
        console.warn('ATTACHMENT CACHE', 'save error', error);
      });
  }

  /**
   * Get attachment id
   * @param url
   * @returns Promise.string cache_id
   */
  getAttachmentCacheId(url) {
    if (!url) {
      return Promise.resolve(null);
    }
    return this.storageClient.getAttachmentCacheId(this.platformName, url)
      .then((result) => {
        if (result && result.cache_id) {
          return result.cache_id;
        }
        return null;
      })
      .catch((error) => {
        console.warn('ATTACHMENT CACHE', 'get error', error);
        return null;
      });
  }

  /**
   * Request listener
   */
  listen() {
    const that = this;

    return (req, res) => {
      if (req.method === 'GET') {
        that.handleGet(req, res);
      } else {
        let requestData = '';
        req.setEncoding('utf8');
        req.on('data', (chunk) => {
          requestData += chunk;
        });
        req.on('end', () => {
          if (!that.validateRequestIntegrity(req, requestData)) {
            res.status(401).send('Error, request integrity check failed');
            return;
          }
          let messageData = {};
          try {
            messageData = JSON.parse(requestData);
          } catch (e) {
            console.warn('Malformed JSON', e, requestData);
            return;
          }
          const mfbMessages = that.reformatRequestData(messageData);

          res.status(200);
          res.end();

          if (mfbMessages.length > 0) {
            that.dispatch(mfbMessages, res);
          }
        });
      }
    };
  }
}

module.exports = BotServiceConnector;
