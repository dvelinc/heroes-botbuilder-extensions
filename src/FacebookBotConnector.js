'use strict';

const crypto = require('crypto');
const tsscmp = require('tsscmp');
const Promise = require('bluebird');
const emoji = require('./emojione');
const BotServiceConnector = require('./BotServiceConnector');

const FB_MESSENGER_ENDPOINT = 'https://graph.facebook.com/v2.6/me/messages';
const FB_SETTINGS_ENDPOINT = 'https://graph.facebook.com/v2.6/me/messenger_profile';

// const FB_PROFILE_ENDPOINT = 'https://graph.facebook.com/v2.6/';

/**
 * Connects Microsoft Bot Framework directly to Facebook Bot API
 * @param settings object { appId, appPassword, verifyToken, secret, pageToken }
 * @param storageClient object new botbuilderExtensions.PostgresClient(config);
 * @param analyticsClients array clients must have a track(data) function
 */
class FacebookBotConnector extends BotServiceConnector {
  constructor(settings, storageClient, analyticsClients) {
    super(settings, storageClient, 'facebook');
    this.analyticsClients = analyticsClients || [];
    if (!Array.isArray(this.analyticsClients)) {
      throw new Error('FacebookBotConnector: analyticsClients must be of type array');
    }
    if (this.analyticsClients) {
      this.analyticsClients.forEach((client) => {
        if (!client.track) {
          throw new Error('FacebookBotConnector: analyticsClients must have a track(date) function');
        }
      });
    }
  }

  validateRequestIntegrity(req, body) {
    const secret = this.settings.secret || '';
    const xHubSignature = req.headers['X-Hub-Signature'] || req.headers['x-hub-signature'] || null;

    if (xHubSignature) {
      const parsedXHubSignature = xHubSignature.split('=');
      const serverSignature = crypto.createHmac(parsedXHubSignature[0], secret).update(body).digest('hex');
      return tsscmp(parsedXHubSignature[1], serverSignature);
    }

    return false;
  }

  handleGet(req, res) {
    if (req.query['hub.mode'] === 'subscribe') {
      const verifyToken = this.settings.verifyToken || '';
      if (req.query['hub.verify_token'] === verifyToken) {
        res.send(req.query['hub.challenge']);
      } else {
        res.send('Error, wrong validation token');
      }
    } else {
      super.handleGet(req, res);
    }
  }

  setProfileSettings(options) {
    const req = {
      url: FB_SETTINGS_ENDPOINT,
      qs: { access_token: this.settings.pageToken },
      method: 'POST',
      json: {
        target_audience: {
          audience_type: 'all',
        },
      },
    };

    if (options.countries && Array.isArray(options.countries)) {
      req.json.target_audience.audience_type = 'custom';
      req.json.target_audience.countries = { whitelist: options.countries };
    }

    if (options.callToActions) {
      req.json.persistent_menu = [
        {
          locale: 'default',
          call_to_actions: options.callToActions,
          composer_input_disabled: options.composer_input_disabled,
        },
      ];
    }

    if (options.greeting) {
      req.json.greeting = [{
        locale: 'default',
        text: options.greeting,
      }];
    }

    if (options.domains && Array.isArray(options.domains)) {
      req.json.whitelisted_domains = options.domains;
    }

    if (options.getStartedPayload) {
      req.json.get_started = {
        payload: options.getStartedPayload,
      };
    }

    return BotServiceConnector.requestp(req);
  }

  postMessage(msg, isLast, cb) {
    this.prepOutgoingMessage(msg);
    // console.log('DEBUG MESSAGE', require('util').inspect(msg, true, 10));
    if (msg.type === 'typing') {
      return this.postTypingIndicator(msg.address.bot && msg.address.bot.id, msg.address.user.id)
        .then(() => {
          cb();
        })
        .catch((error) => {
          cb(error.error ? error.error.message : error);
        });
    }
    const fbMessage = {};

    if (msg.text) {
      msg.text = emoji.convertEmojiShortnamesToUnicode(msg.text);
      fbMessage.text = msg.text;
    }

    if (msg.channelData) {
      if (msg.channelData.quick_replies) {
        msg.channelData.quick_replies.forEach((quickReply) => {
          quickReply.title = emoji.convertEmojiShortnamesToUnicode(quickReply.title);
        });
        fbMessage.quick_replies = msg.channelData.quick_replies;
      }

      if (msg.channelData.attachment) {
        fbMessage.attachment = msg.channelData.attachment;
      }
    }
    if (!fbMessage.attachment && msg.attachments && Array.isArray(msg.attachments) && msg.attachments.length > 0) {
      if (fbMessage.text) {
        return this.postMessageFB(msg.address.user.id, msg.address.bot && msg.address.bot.id, { text: fbMessage.text })
          .then(() => {
            return this.postAttachmentsFB(
              msg.address.user.id,
              msg.address.bot && msg.address.bot.id,
              msg.attachmentLayout,
              msg.attachments,
              fbMessage.quick_replies ? fbMessage.quick_replies : null,
              cb);
          })
          .catch((error) => {
            cb(error.error ? error.error.message : error);
          });
      }
      return this.postAttachmentsFB(
        msg.address.user.id,
        msg.address.bot && msg.address.bot.id,
        msg.attachmentLayout,
        msg.attachments,
        fbMessage.quick_replies ? fbMessage.quick_replies : null,
        cb
      );
    }
    return this.postMessageFB(msg.address.user.id, msg.address.bot && msg.address.bot.id, fbMessage)
      .then(() => {
        cb();
      })
      .catch((error) => {
        cb(error.error ? error.error.message : error);
      });
  }

  postAttachmentsFB(recipientId, botId, layout, attachments, quickReplies, cb) {
    const that = this;
    if (layout === 'carousel') {
      const fbMessage = {
        attachment: {
          type: 'template',
          payload: {
            template_type: 'generic',
            elements: [],
          },
        },
      };
      attachments.forEach((attachment) => {
        fbMessage.attachment.payload.elements.push(FacebookBotConnector.prepareElement(attachment.content));
      });

      if (quickReplies) {
        fbMessage.quick_replies = quickReplies;
      }
      return this.postMessageFB(recipientId, botId, fbMessage)
        .then(() => {
          cb();
        })
        .catch((error) => {
          cb(error.error ? error.error.message : error);
        });
    }
    let curAttachment = 0;
    const newAttachments = [];
    return Promise.each(attachments, (attachment) => {
      curAttachment += 1;
      const fbMessage = {};

      if (attachment.contentType.substr(0, 5) === 'image' || attachment.contentType.substr(0, 5) === 'video' ||
        attachment.contentType.substr(0, 5) === 'audio') {
        if (typeof attachment.contentUrl !== 'string') {
          fbMessage.filedata = attachment.contentUrl;
        } else {
          fbMessage.attachment = {
            type: attachment.contentType.substr(0, 5),
            payload: { url: attachment.contentUrl },
          };
          if (attachment.reusable) {
            fbMessage.attachment.payload.is_reusable = true;
          }
        }
      } else if (attachment.contentType === 'application/vnd.microsoft.card.hero' ||
        attachment.contentType === 'application/vnd.microsoft.card.thumbnail') {
        fbMessage.attachment = {
          type: 'template',
          payload: {
            template_type: 'generic',
            elements: [],
          },
        };

        fbMessage.attachment.payload.elements.push(FacebookBotConnector.prepareElement(attachment.content));
      } else if (attachment.contentType === 'application/vnd.microsoft.card.video') {
        if (attachment.content.media &&
          Array.isArray(attachment.content.media) &&
          attachment.content.media.length > 0) {
          fbMessage.attachment = {
            type: 'video',
            payload: { url: attachment.content.media[0].url },
          };

          if (attachment.reusable) {
            fbMessage.attachment.payload.is_reusable = true;
          }

          if (attachment.content.title) {
            const videoMeta = JSON.parse(JSON.stringify(attachment)); // clone attachment
            videoMeta.contentType = 'application/vnd.microsoft.card.hero';
            delete videoMeta.image;
            delete videoMeta.media;
            delete videoMeta.autoloop;
            delete videoMeta.autostart;

            newAttachments.push(videoMeta);
          }
        }
      }

      // attach quick replies if this is the last attachment
      if (quickReplies && curAttachment === attachments.length && newAttachments.length === 0) {
        fbMessage.quick_replies = quickReplies;
      }

      let url;
      if (!fbMessage.filedata && fbMessage.attachment.payload.is_reusable) {
        url = fbMessage.attachment.payload.url ? fbMessage.attachment.payload.url : null;
      }

      return that.getAttachmentCacheId(url)
        .then((attachmentId) => {
          if (attachmentId) {
            fbMessage.attachment.payload = { attachment_id: attachmentId };
          }
          return this.postMessageFB(recipientId, botId, fbMessage);
        });
    })
      .catch((error) => {
        if (newAttachments.length > 0) {
          return this.postAttachmentsFB(recipientId, botId, null, newAttachments, quickReplies, cb);
        }
        cb(error);
        return null;
      });
  }

  static prepareElement(attachmentContent) {
    const element = {};

    if (attachmentContent.title) {
      element.title = emoji.convertEmojiShortnamesToUnicode(attachmentContent.title);
    }

    if (attachmentContent.subtitle) {
      element.subtitle = emoji.convertEmojiShortnamesToUnicode(attachmentContent.subtitle);
    }

    if (attachmentContent.tap && attachmentContent.tap.type === 'openUrl') {
      element.default_action = {
        type: 'web_url',
        url: attachmentContent.tap.value,
      };

      if (attachmentContent.tap.messenger_extensions) {
        element.default_action.messenger_extensions = attachmentContent.tap.messenger_extensions;
      }

      if (attachmentContent.tap.webview_height_ratio) {
        element.default_action.webview_height_ratio = attachmentContent.tap.webview_height_ratio;
      }

      if (attachmentContent.tap.fallback_url) {
        element.default_action.fallback_url = attachmentContent.tap.fallback_url;
      }
    }

    if (attachmentContent.images &&
      Array.isArray(attachmentContent.images) &&
      attachmentContent.images.length > 0) {
      element.image_url = attachmentContent.images[0].url;
    }

    if (attachmentContent.buttons &&
      Array.isArray(attachmentContent.buttons) &&
      attachmentContent.buttons.length > 0) {
      element.buttons = [];
      attachmentContent.buttons.forEach((button) => {
        switch (button.type) {
          case 'imBack':
          case 'postBack': {
            element.buttons.push({
              type: 'postback',
              title: emoji.convertEmojiShortnamesToUnicode(button.title),
              payload: button.value,
            });
            break;
          }
          case 'shareCard': {
            element.buttons.push({
              type: 'element_share',
            });
            break;
          }
          case 'openUrl': {
            const fbButton = {
              type: 'web_url',
              title: emoji.convertEmojiShortnamesToUnicode(button.title),
              url: button.value,
            };

            if (button.messenger_extensions) {
              fbButton.messenger_extensions = button.messenger_extensions;
            }

            if (button.webview_height_ratio) {
              fbButton.webview_height_ratio = button.webview_height_ratio;
            }

            if (button.fallback_url) {
              fbButton.fallback_url = button.fallback_url;
            }

            element.buttons.push(fbButton);
            break;
          }
          default:
            break;
        }
      });
    }

    return element;
  }

  postMessageFB(recipientId, botId, fbMessage) {
    const that = this;
    return this.storageClient.getClientByBotId(botId)
      .then((client) => {
        let accessToken = this.settings.pageToken;
        if (client) {
          accessToken = client.page_token;
        }
        const options = {
          url: FB_MESSENGER_ENDPOINT,
          qs: { access_token: accessToken },
          method: 'POST',
        };
        if (fbMessage.filedata) { // sending file buffers directly to FB
          options.formData = {
            // TODO use real type? https://developers.facebook.com/docs/messenger-platform/send-messages#send_api_basics
            messaging_type: 'RESPONSE',
            recipient: JSON.stringify({ id: String(recipientId) }),
            message: JSON.stringify({
              attachment: {
                type: 'image',
                payload: {},
              },
            }),
            filedata: fbMessage.filedata,
          };
        } else {
          options.json = {
            // TODO use real type? https://developers.facebook.com/docs/messenger-platform/send-messages#send_api_basics
            messaging_type: 'RESPONSE',
            recipient: { id: String(recipientId) },
            message: fbMessage,
            notification_type: 'REGULAR',
          };
        }
        const request = BotServiceConnector.requestp(options);

        if (fbMessage.attachment && fbMessage.attachment.payload && fbMessage.attachment.payload.is_reusable) {
          request.then((body) => {
            if (body.attachment_id) {
              that.saveAttachmentId(fbMessage.attachment.payload.url, body.attachment_id);
            }
            return body;
          });
        }

        return request;
      })
      .catch((error) => {
        console.error('FacebookBotConnector.postMessageFB', error);
      });
  }

  postTypingIndicator(botId, recipientId) {
    return this.storageClient.getClientByBotId(botId)
      .then((client) => {
        let accessToken = this.settings.pageToken;
        if (client) {
          accessToken = client.page_token;
        }
        return BotServiceConnector.requestp({
          url: FB_MESSENGER_ENDPOINT,
          qs: { access_token: accessToken },
          method: 'POST',
          json: {
            recipient: { id: String(recipientId) },
            sender_action: 'typing_on',
            notification_type: 'REGULAR',
          },
        });
      });
  }

  reformatRequestData(messageData) {
    const mfbMessages = [];
    this.doAnalytics(messageData);

    messageData.entry.forEach((entry) => {
      entry.messaging.forEach((event) => {
        const mfbMessage = {
          type: 'unknown',
          useAuth: true,
          recipient: { id: entry.id },
          channelData: event,
          channelId: 'facebook',
          from: { id: event.sender.id },
          timestamp: new Date().toISOString(),
          conversation: { isGroup: false, id: `${event.sender.id}-${entry.id}` },
        };

        if (event.message && !event.message.is_echo) {
          mfbMessage.id = event.message.mid;
          mfbMessage.type = 'message';
          if (event.message.quick_reply && event.message.quick_reply.payload) {
            mfbMessage.text = event.message.quick_reply.payload; // mfb doesn't send payload, but text
          } else if (event.message.text) {
            mfbMessage.text = event.message.text;
          } else if (event.message.attachments) {
            mfbMessage.entities = [];
            mfbMessage.attachments = [];
            event.message.attachments.forEach((attachment) => {
              if (attachment.type === 'image') {
                const imageType = attachment.payload.url.split('.').pop().split(/#|\?/)[0];
                mfbMessage.attachments.push({
                  contentType: `image/${imageType}`,
                  contentUrl: attachment.payload.url,
                });
              } else if (attachment.type === 'location') {
                mfbMessage.entities.push({
                  geo: {
                    elevation: 0,
                    latitude: attachment.payload.coordinates.lat,
                    longitude: attachment.payload.coordinates.long,
                    type: 'GeoCoordinates',
                  },
                  type: 'Place',
                });
              } else if (attachment.type === 'fallback') {
                console.warn('Attachment not found', JSON.stringify(attachment));
              }
            });
          }
        } else if (event.message && event.message.is_echo) {
          mfbMessage.type = 'echo';
          mfbMessage.from = { id: event.sender.id };
          mfbMessage.recipient = { id: event.recipient.id };
          mfbMessage.conversation = { isGroup: false, id: `${event.recipient.id}-${entry.id}` };
        } else if (event.referral) {
          mfbMessage.type = 'referral';
          mfbMessage.text = event.referral.ref;
        } else if (event.read) {
          mfbMessage.type = 'read';
        } else if (event.delivery) {
          mfbMessage.type = 'delivery';
        } else if (event.postback) {
          mfbMessage.type = 'message';
          mfbMessage.id = 'noid';
          mfbMessage.text = event.postback.payload;
          mfbMessage.value = event.postback.payload;
        } else if (event.account_linking) {
          mfbMessage.type = 'account_link';
          mfbMessage.status = event.account_linking.status;
          mfbMessage.code = event.account_linking.authorization_code;
        }

        this.prepIncomingMessage(mfbMessage);

        mfbMessages.push(mfbMessage);
      });
    });

    return mfbMessages;
  }

  doAnalytics(messageData) {
    if (this.analyticsClients) {
      this.analyticsClients.forEach((client) => {
        client.track(messageData)
          .catch((error) => {
            console.error('FacebookBotConnector.doAnalytics.bonobo', JSON.stringify(messageData), error.message);
          });
      });
    }
  }

  send(messages, done) {
    const that = this;
    const addresses = [];
    return Promise.each(messages, (message, index) => {
      if (!message.address || !message.address.user || !message.address.user.id) {
        throw new Error('FacebookBotConnector.send: message is missing address or user id.');
      }
      return that.postMessage(message, (index === messages.length - 1), (error, address) => {
        addresses.push(address);
      });
    })
      .then(() => {
        done(null, addresses);
      })
      .catch((error) => {
        console.error('FacebookBotConnector.send', error);
        done(error);
      });
  }

  startConversation(address, done) { // eslint-disable-line class-methods-use-this
    if (!address || !address.user || !address.user.id) {
      done('FacebookBotConnector.startConversation: address is missing user id.');
    }
    address.conversation = { id: address.user.id };
    done(null, address);
  }
}

module.exports = FacebookBotConnector;
