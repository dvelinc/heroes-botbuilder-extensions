'use strict';

const emojione = require('./emojione-lib');

/**
 * converts all emojis in an given text into :shortnames:
 * @param string
 * @returns {string}
 */
exports.convertEmojisToShortnames = function convertEmojisToShortnames(string) {
  if (!string) return '';
  return emojione.toShort(string);
};

/**
 * converts all emoji-shortnames in an given text into unicodes
 * @param string
 * @returns {string}
 */
exports.convertEmojiShortnamesToUnicode = function convertEmojiShortnamesToUnicode(string) {
  if (!string) return '';
  return emojione.shortnameToUnicode(string);
};
