'use strict';

const constants = require('./CONST');

function BotStorage(storageClient) {
  this.storageClient = storageClient;
}

BotStorage.prototype.client = function client(storageClient) {
  this.storageClient = storageClient;
  return this;
};

BotStorage.prototype.getData = function getData(context, callback) {
  // console.time(`session get ${context.address.user.id}`);
  const data = {};
  this.storageClient.getStorageData(context.address.channelId, context.address.user.id)
    .then((entity) => {
      // console.timeEnd(`session get ${context.address.user.id}`);
      if (entity) {
        data[constants.Fields.IdField] = entity.id;
        data[constants.Fields.PrivateConversationDataField] = entity.private_conversation_data;
        data[constants.Fields.PrivateConversationDataField + constants.hash] =
          entity.private_conversation_data ? JSON.stringify(entity.private_conversation_data) : null;
        data[constants.Fields.UserDataField] = entity.user_data ? entity.user_data : {};
        data[constants.Fields.UserDataField].pgUserId = entity.id; // == postgres user ID
        data[constants.Fields.UserDataField + constants.hash] =
          entity.user_data ? JSON.stringify(entity.user_data) : null;
        callback(null, data);
      } else {
        data[constants.Fields.IdField] = null;
        data[constants.Fields.PrivateConversationDataField] = null;
        data[constants.Fields.PrivateConversationDataField + constants.hash] = null;
        data[constants.Fields.UserDataField] = null;
        data[constants.Fields.UserDataField + constants.hash] = null;
        callback(null, data);
      }
    })
    .catch((error) => {
      callback(error, data);
    });
};

BotStorage.prototype.saveData = function saveData(context, data, callback) {
  // console.time(`session save ${context.address.user.id}`);
  delete data.userData.pgUserId; // == remove postgres user ID we dont need to persistent it
  this.storageClient.saveStorageData(context.address.channelId, context.address.user.id, data)
    .then(() => {
      // console.timeEnd(`session save ${context.address.user.id}`);
      callback(null);
    })
    .catch((error) => {
      callback(error);
    });
};

module.exports = BotStorage;
