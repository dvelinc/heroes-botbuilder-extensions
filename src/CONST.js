'use strict';

const CONST = {
  Fields: {
    IdField: 'userId',
    UserDataField: 'userData',
    ConversationDataField: 'conversationData',
    PrivateConversationDataField: 'privateConversationData',
  },
  userTableName: 'chat_bot_users',
  instagramUserTableName: 'instagram_users',
  messengerCodeTableName: 'messenger_codes',
  clientsTableName: 'clients',
  historyTableName: 'chat_bot_message_history',
  attachmentCacheTableName: 'chat_bot_attachment_cache',
  hash: 'Hash',
};

module.exports = CONST;
